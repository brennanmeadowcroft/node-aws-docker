FROM node:12-alpine
RUN apk add --update \
    python3 \
    python-dev \
    py-pip \
    build-base \
    && pip install awscli --upgrade \
    && rm -rf /var/cache/apk/*
RUN aws --version
RUN node --version